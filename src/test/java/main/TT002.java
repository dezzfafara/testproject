package main;

import environment.Environment;
import org.testng.annotations.Test;

public class TT002 extends BaseTest {

    @Test(description = "TT002")
    public void testTT002() {
        openApp();
        mainPage.setSearchText(Environment.getInstance().getDefaultSearchValue());

        System.out.println("COUNT OF RESULTS: " + mainPage.getCountOfResults());
    }
}
