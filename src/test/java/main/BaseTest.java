package main;

import environment.Environment;
import factory.DriverManager;
import factory.DriverManagerFactory;
import factory.DriverType;
import interceptors.SomeStaticWebDriver;
import interceptors.TestFilterInterceptor;
import listeners.TestListener;
import modules.DriverModule;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;
import org.testng.annotations.Listeners;
import ui.pages.MainPage;

@Guice(modules = {
        DriverModule.class
})

@Listeners({TestFilterInterceptor.class, TestListener.class})
public class BaseTest {
    protected DriverManager driverManager;
    //@Inject
    //protected WebDriver driver;

    protected MainPage mainPage;

    @BeforeClass(alwaysRun = true)
    protected final void beforeClass(ITestContext iTestContext) {
        driverManager = DriverManagerFactory.getManager(DriverType.CHROME);
        driverManager.getDriver().manage().window().maximize();
        driverManager.getDriver().manage().deleteAllCookies();

        SomeStaticWebDriver.driver = driverManager.getDriver();

        mainPage = new MainPage(driverManager.getDriver());
    }

    @AfterClass(alwaysRun = true)
    protected void afterClass() {
        driverManager.quitDriver();
        SomeStaticWebDriver.driver = null;
    }

    public void openApp() {
        driverManager.getDriver().get(Environment.getInstance().getBaseUrl());
    }

}
