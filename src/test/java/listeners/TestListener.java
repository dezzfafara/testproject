package listeners;

import interceptors.SomeStaticWebDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;

public class TestListener implements ITestListener {
    private final static String SCREENSHOTS_PATH = "./target/screenshots/";

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("***** FAILURE " + result.getName() + " *****");
        String methodName = result.getName().trim();
        takeScreenShot(methodName, SomeStaticWebDriver.driver);
    }

    public void takeScreenShot(String methodName, WebDriver driver) {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        //The below method will save the screen shot in d drive with test method name
        try {
            FileUtils.copyFile(scrFile, new File(SCREENSHOTS_PATH + scrFile.getName()));
            System.out.println("***Placed screen shot in " + SCREENSHOTS_PATH + " ***");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onFinish(ITestContext context) {
    }

    public void onTestStart(ITestResult result) {
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("***** SUCCESS " + result.getName() + " *****");
        String methodName = result.getName().trim();
        takeScreenShot(methodName, SomeStaticWebDriver.driver);
    }

    public void onTestSkipped(ITestResult result) {
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    public void onStart(ITestContext context) {
    }
}