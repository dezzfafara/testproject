package ui.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    protected WebDriver driver;
    private WebDriverWait wait;

    @Inject
    protected BasePage(WebDriver driver) {
        this.driver = driver;
        decorateElements();

        wait = new WebDriverWait(driver, 10);
    }

    protected void decorateElements() {
        PageFactory.initElements(driver, this);
    }

    protected void waitForElement(WebElement element) {
        wait.ignoring(WebDriverException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }
}
