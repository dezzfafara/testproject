package ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MainPage extends BasePage {
    @FindBy(css = ".Header__FindTxtInput")
    private WebElement searchField;

    @FindBy(css = ".Header__SearchHelpCont")
    private WebElement searchResultsBlock;

    @FindBy(css = ".FindTips__ModelBlock.FindTips__BlockItemListRes")
    private List<WebElement> searchResults;

    //Menus
    @FindBy(css = ".Header__ButtonCatalog.Header__CatalogDescShow.visible-lg")
    private WebElement menu;

    @FindBy(css = ".DesktopMenu")
    private WebElement desktopMenu;

    @FindBy(css = ".Page__SectionIco.Page__SectionIco736")
    private WebElement computers;
    @FindBy(xpath = "(//a[contains(@href,'/kompyutery/kompyuternaya_periferiya/')])[1]")
    private WebElement periphery;
    @FindBy(xpath = "//a[contains(@href, 'web_kamery')]")
    private WebElement cameras;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public void setSearchText(String text) {
        searchField.sendKeys(text);
    }

    public String getSearchText() {
        return searchField.getAttribute("value");
    }

    public int getCountOfResults() {
        waitForElement(searchResultsBlock);
        return searchResults.size();
    }

    public void selectWebCamerasInMenu() {
        Actions actions = new Actions(driver);
        waitForElement(menu);
        actions.moveToElement(menu).perform();
        waitForElement(desktopMenu);
        waitForElement(computers);
        actions.moveToElement(computers).perform();
        waitForElement(periphery);
        actions.moveToElement(periphery).perform();
        waitForElement(cameras);
        cameras.click();

        System.out.println("WEB CAMERAS CATALOG IS OPENED");
    }
}
