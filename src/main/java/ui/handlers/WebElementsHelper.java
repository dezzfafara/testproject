package ui.handlers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WebElementsHelper {
    public static List<WebElement> getChildren(WebElement parentElement, By locator) {
        return parentElement.findElements(locator);
    }

    public static WebElement getChild(WebElement parentElement, By locator) {
        return getChildren(parentElement, locator).get(0);
    }
}
