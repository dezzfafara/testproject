package interceptors;

import org.testng.IMethodInstance;

import java.util.Comparator;

public class TestMethodsByClassNameComparator implements Comparator<IMethodInstance> {
    @Override
    public int compare(IMethodInstance o1, IMethodInstance o2) {
        return o1.getMethod().getInstance().getClass().getSimpleName().toLowerCase()
                .compareTo(o2.getMethod().getInstance().getClass().getSimpleName().toLowerCase());
    }
}
