package factory;

public class DriverManagerFactory {

    public static DriverManager getManager(DriverType type) {

        DriverManager driverManager;

        switch (type) {
            case FIREFOX:
                driverManager = new FirefoxDriverManager();
                break;
            case CHROME:
            default:
                driverManager = new ChromeDriverManager();
        }
        return driverManager;
    }
}
