package environment;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class Environment {
    private static Environment environment;

    private String baseUrl;
    private String defaultSearchValue;

    private Environment() {

        Properties properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(Environment.class.getClassLoader().getResourceAsStream("properties/" + "app.properties")));
        } catch (IOException e) {
            throw new RuntimeException("Error during loading properties file");
        }

        this.baseUrl = properties.getProperty("baseUrl");
        this.defaultSearchValue = properties.getProperty("defaultSearchValue");
    }

    public static Environment getInstance() {
        if (null == environment)
            environment = new Environment();

        return environment;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getDefaultSearchValue() {
        return defaultSearchValue;
    }
}
